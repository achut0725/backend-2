const jwt = require("jsonwebtoken");

async function verifyToken(req, res, next) {
  console.log(req.cookies);

  console.log("verify route called");

  const authHeader = req.headers["authorization"];

  let token = null;

  if (authHeader) {
    const authHeaderToArray = authHeader.split(" ");

    token = authHeaderToArray[1];
  }

  if (token == null) return res.sendStatus(401);
  console.log("error");

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    console.log(err);

    if (err) return res.sendStatus(403);

    req.user = user;

    next();
  });
}

function roleCheck(req, res, next) {
  console.log(req.user);
  if (req.user.role === "admin") {
    next();
  } else {
    res.status(403).send("forbidden");
  }
}

module.exports = {
  verifyToken,
  roleCheck,
};
