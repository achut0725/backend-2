const Screening = require("../models/screening");

const getAllScreenings = async (req, res) => {
    console.log(req.query);
    try {
      const screenings = await Screening.find(req.query).populate("screen movie");
      res.json(screenings);
    } catch (err) {
      res.send("Error " + err);
    }
  }

  const getScreeningByscreeningId = async (req, res) => {
    console.log(req.params);
    try {
      const screenings = await Screening.findById(
        req.params.screeningId
      ).populate("screen");
      res.json(screenings);
    } catch (err) {
      res.send("Error " + err);
    }
  }

  const getScreeningById = async (req, res) => {
    try {
      const screenings = await Screening.findById(req.params.id);
    } catch (err) {
      res.send("Error ", +err);
    }
  }

  const postScreening = async (req, res) => {
    const screenings = new Screening({
      movie: req.body.movie,
      screen: req.body.screen,
      time: req.body.time,
    });
    try {
      const a1 = await screenings.save();
      console.log(a1);
      res.json(a1);
    } catch (err) {
      console.log(err);
      res.send("Error.......");
    }
  }

  const cancelScreeningById = async (req, res)=> {
    try {
      const screening = await Screening.findByIdAndUpdate(
        req.params.id,
        { status: false },
        { new: true }
      );
      res.json(screening);
    } catch(err) {
      console.log("Error while patching ",err);
      res.render("Error while patching");
    }
  }

  const deleteScreeningById = async (req, res) => {
    try {
      const screenings = await Screening.findByIdAndDelete(req.params.id);
      const a1 = await screenings.remove();
      res.json(a1);
      res.send("Deleted");
    } catch (err) {
      res.send("Error in deleting");
    }
  }


const deleteScreeningBySreen = async (req, res) => {
  try {
    const screenings = await Screening.deleteMany(req.body.screen.screenNumber);
    const a1 = await screenings.remove();
    res.json(a1);
  } catch (err) {
    res.send("Error in deleting");
  }
}

  const deleteAllScreenings = async (req, res) => {
    const deleteAll = await Screening.deleteMany();
    res.send("All Screenings have been deleted");
  }

  module.exports = {
    getAllScreenings,
    getScreeningByscreeningId,
    getScreeningById,
    postScreening,
    cancelScreeningById,
    deleteScreeningById,
    deleteScreeningBySreen,
    deleteAllScreenings,
  }