const Screen = require("../models/screen");

const getScreens = async (req, res) => {
  try {
    let query = {};

    if (req.query.status === "true") {
      query.status = true;
    }
    const screens = await Screen.find(req.query);
    res.json(screens);
  } catch (err) {
    res.status(500).send("Error: " + err);
  }
};

const getScreenById = async (req, res) => {
  try {
    const screens = await Screen.findById(req.params.id);
    res.json(screens);
  } catch (err) {
    res.status(404).send("Error ");
  }
};

const postScreen = async (req, res) => {
  console.log(req.body);
  const screens = new Screen({
    screenNumber: req.body.screenNumber,
    rows: req.body.rows,
    price: req.body.price,
  });
  try {
    const a1 = await screens.save();
    console.log(a1);
    res.json(a1);
  } catch (err) {
    console.log(err);
    res.send("Error.......");
  }
};

const removeScreen = async (req, res, next) => {
  try {
    const screen = await Screen.findByIdAndUpdate(
      req.params.id,
      { status: false },
      { new: true }
    );
    next();
  } catch (err) {
    console.log("Error while patching ", err);
    res.render("Error while patching");
  }
};

const deleteScreenById = async (req, res, next) => {
  try {
    const screens = await Screen.findByIdAndDelete(req.params.id);
    next();
  } catch (err) {
    res.send("Error in deleting");
  }
};

const deleteAllScreens = async (req, res) => {
  const deleteAll = await Screen.deleteMany();
  res.send("All Screens have been deleted");
};


module.exports = {
  getScreens,
  getScreenById,
  postScreen,
  removeScreen,
  deleteScreenById,
  deleteAllScreens,
};
