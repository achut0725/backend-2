const Movie = require("../models/movie");

const getMovies = async (req, res) => {
  try {
    let query = {};

    if (req.query.status === "true") {
      query.status = true;
    }
    const movies = await Movie.find(req.query)
      .populate({
        path: "castMembers",
        populate: [
          {
            path: "cast",
            model: "CrewMember",
          },
        ],
      })
      .populate({
        path: "crewMembers",
        populate: [
          {
            path: "crew",
            model: "CrewMember",
          },
        ],
      });
    res.json(movies);
  } catch (err) {
    res.status(500).send("Error: " + err);
  }
};

const getMoviesById = async (req, res) => {
  try {
    const movies = await Movie.findById(req.params.id)
      .populate({
        path: "castMembers",
        populate: [
          {
            path: "cast",
            model: "CrewMember",
          },
        ],
      })
      .populate({
        path: "crewMembers",
        populate: [
          {
            path: "crew",
            model: "CrewMember",
          },
        ],
      });
    res.json(movies);
  } catch (err) {
    res.status(500).send("Error: " + err);
  }
};

const postMovies = async (req, res) => {
  const movies = new Movie({
    movieName: req.body.movieName,
    discription: req.body.discription,
    director: req.body.director,
    script: req.body.script,
    duration: req.body.duration,
    summery: req.body.summery,
    image: req.body.image,
    backgroundImage: req.body.backgroundImage,
    rating: req.body.rating,
    moreInfo: req.body.moreInfo,
    castMembers: req.body.castMembers,
    crewMembers: req.body.crewMembers,
  });
  try {
    const a1 = await movies.save();
    console.log(a1);
    res.json(a1);
  } catch (err) {
    console.log(err);
    res.send("Error.......");
  }
};

const putAnyChangesInMovieDetails = async (req, res) => {
  try {
    console.log(req.body);
    let movies = await Movie.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.json(movies);
    // res.send({movies: "Updated"})
  } catch (err) {
    console.log("Error while putting data" + err);
  }
};

const removeMovie = async (req, res, next) => {
  try {
    const movie = await Movie.findByIdAndUpdate(
      req.params.id,
      { status: false },
      { new: true }
    );
    next();
  } catch (err) {
    console.log("Error while patching ", err);
    res.render("Error while patching");
  }
};

const deleteMovieById = async (req, res, next) => {
  try {
    const movies = await Movie.findByIdAndDelete(req.params.id);
    next();
  } catch (err) {
    res.send("Error in deleting");
  }
};

const deleteAllMovies = async (req, res) => {
  const deleteAll = await Movie.deleteMany();
  res.send("All movies have been deleted");
};

module.exports = {
  getMovies,
  getMoviesById,
  postMovies,
  putAnyChangesInMovieDetails,
  deleteMovieById,
  deleteAllMovies,
  removeMovie,
};
