const User = require("../models/user");
const Ticket = require("../models/tickets");


const postBooking = async (req, res) => {
  const tickets = new Ticket({
    user: req.user._id,
    screening: req.body.screening,
    seatsSelected: req.body.seatsSelected,
    price: req.body.price
  });

  try {
    const a1 = await tickets.save();
    res.json(a1);
  } catch (err) {
    console.log(err);
    res.send("Error in Posting");
  }
}

const getBookingForAdmin = async (req, res) => {
    try {
      const tickets = await Ticket.find(req.query)
        .populate("user")
        .populate({
          path: "screening",
          populate: [
            {
              path: "movie",
              model: "Movie",
            },
            {
              path: "screen",
              model: "Screen",
            },
          ],
        });
      res.json(tickets);
    } catch (err) {
      res.send("Error " + err);
    }
}

const putAnyChangesInTicket = async (req, res) => {
    try{
      console.log(req.body)
      let ticket = await Ticket.findByIdAndUpdate( req.params.id, req.body, {new: true} );
      res.json(ticket)
      // res.send({movies: "Updated"})
    }catch(err) {
      console.log("Error while putting data" + err)
    }
  }

const cancelBookingForAdmin = async (req, res) => {
    try {
      const ticket = await Ticket.findByIdAndUpdate(
        req.params.id,
        { bookingStatus: false }, { new: true }
      );
      res.json(ticket);
    } catch (err) {
      console.log(err);
      res.send("Error in patching");
    }
  }

const cancelBookingForUser = async (req, res) => {
    try {
      const ticket = await Ticket.findByIdAndUpdate(
        req.params.id,
        { bookingStatus: false }, { new: true, }
      );
      res.json(ticket);
    } catch (err) {
      console.log(err);
      res.send("Error in patching");
    }
}

const getBookingForUser = async (req, res) => {
    try {
      const tickets = await Ticket.find({
        user: req.user._id,
        paymentReference: { $exists: true },
      })
        .populate({ path: "screening", populate: "movie screen" })
        .populate("user");
      res.json(tickets);
    } catch (err) {
      res.send("Error ");
    }
}

const getBookedTickets = async (req, res) => {
    try {
      console.log(req.query);
      const tickets = await Ticket.find(req.query).select("seatsSelected");
      const seatsSelected = [];
  
      tickets.forEach((ticket) => {
        ticket.seatsSelected.forEach((seat) => {
          seatsSelected.push(seat);
        });
      });
      res.json(seatsSelected);
    } catch (err) {
      res.send("Error " + err);
    }
  }

const getBookingsById = async (req, res) => {
    try {
      const tickets = await Ticket.findById(req.params.id)
      .populate("user")
      .populate({
        path: "screening",
        populate: [
          {
            path: "movie",
            model: "Movie",
          },
          {
            path: "screen",
            model: "Screen",
          },
        ],
      });
      res.json(tickets);
    } catch (err) {
      res.send("Error " + err);
    }
  }

const patchAnythingInTicket = async (req, res) => {
    try {
      const tickets = await Ticket.findById(req.params.id);
      tickets.status = req.body.status;
      const a1 = await tickets.save();
      res.json(a1);
    } catch (err) {
      res.send("Error in patching");
    }
  }

const deleteBookingById = async (req, res) => {
    try {
      const tickets = await Ticket.findById(req.params.id);
      tickets.status = req.body.status;
      const a1 = await tickets.remove();
      res.json(a1);
    } catch (err) {
      res.send("Error in deleting");
    }
  }

const deleteAllBookings = async (req, res) => {
    const deleteAll = await Ticket.remove();
    res.send("All Tickets have been deleted");
  }

module.exports = {
    postBooking,
    getBookingForAdmin,
    putAnyChangesInTicket,
    cancelBookingForAdmin,
    cancelBookingForUser,
    getBookingForUser,
    getBookedTickets,
    getBookingsById,
    patchAnythingInTicket,
    deleteBookingById,
    deleteAllBookings,
}