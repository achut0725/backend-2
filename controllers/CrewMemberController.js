const CrewMember = require("../models/crewMember")


const getCrewMembers = async (req, res) => {
    try {
      const crewMembers = await CrewMember.find();
      res.json(crewMembers);
    } catch (err) {
      res.send("Error " + err);
    }
}

const getCrewMembersById = async (req, res) => {
    try {
      const crewMembers = await CrewMember.findById(req.params.id);
      res.json(crewMembers);
    } catch (err) {
      res.send("Error " + err);
    }
}

const postCrewMembers = async (req, res) => {
    const crewMembers = new CrewMember({
      name: req.body.name,
      bio: req.body.bio,
      image: req.body.image,
    });
    try {
      const a1 = await crewMembers.save();
      console.log(a1);
      res.json(a1);
    } catch (err) {
      console.log(err);
      res.send("Error while posting");
    }
}

const putAnyChangesInCrewMembersDetails = async (req, res) => {
    try{
      console.log(req.body)
      let crewMembers = await CrewMember.findByIdAndUpdate(req.params.id, req.body, {new: true} );
      res.json(crewMembers)
      // res.send({movies: "Updated"})
    }catch(err) {
      console.log("Error while putting data" + err)
    }
  }

const deleteCrewMembers = async (req, res) => {
    try {
      const crewMembers = await CrewMember.findByIdAndDelete(req.params.id);
      const a1 = await crewMembers.remove();
      res.json(a1);
    } catch (err) {
      res.send("Error in deleting");
    }
}


module.exports = {
    getCrewMembers,
    getCrewMembersById,
    postCrewMembers,
    putAnyChangesInCrewMembersDetails,
    deleteCrewMembers,
}