const EventHead = require("../models/eventHead")


const getEventHeads = async (req, res) => {
    try {
      const eventHeads = await EventHead.find();
      res.json(eventHeads);
    } catch (err) {
      res.send("Error " + err);
    }
}

const getEventHeadsById = async (req, res) => {
    try {
      const eventHeads = await EventHead.findById(req.params.id);
      res.json(eventHeads);
    } catch (err) {
      res.send("Error " + err);
    }
  }

const postEventHeads = async (req, res) => {
    const eventHeads = new EventHead({
      eventHeadName: req.body.eventHeadName,
      discription: req.body.discription,
      image: req.body.image,
    //   events: req.body.events,
    });
    try {
      const a1 = await eventHeads.save();
      console.log(a1);
      res.json(a1);
    } catch (err) {
      console.log("Error while posting eventHeads" +err);
      res.send("Error while posting eventHeads");
    }
}

const deleteEventHeadsById = async (req, res) => {
    try {
      const eventHeads = await EventHead.findByIdAndDelete(req.params.id);
      // movies.discription = req.body.discription
      const a1 = await eventHeads.remove();
      // screening.remove({movie_id: this._id}).exec()
      res.json(a1);
    } catch (err) {
      res.send("Error in deleting");
    }
  }


module.exports = {
    getEventHeads,
    getEventHeadsById,
    postEventHeads,
    deleteEventHeadsById,
}