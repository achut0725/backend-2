const User = require("../models/user");

const bcrypt = require("bcrypt");
const saltRounds = 10;

const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

// get config vars
dotenv.config();
// access config var
process.env.TOKEN_SECRET;

const getUsers = async (req, res) => {
    try {
      const users = await User.find();
      res.json(users);
    } catch (err) {
      res.send("Error " + err);
    }
  }

const getUsersForUser = async (req, res) => {
    try {
      const user = await User.findById(req.user._id);
      res.json(user);
    } catch (err) {
      res.send("Error ");
    }
  }
  
const getUserById = async (req, res) => {
    try {
      const users = await User.findById(req.params.id);
      res.json(users);
    } catch (err) {
      res.send("Error " + err);
    }
  }

const postUser = async (req, res) => {
    try {
      // Check if the userName already exists
      const existingUser = await User.findOne({ userName: req.body.userName });
      if (existingUser) {
        // User with the same userName already exists
        return res.status(400).send("Username already exists");
      }
  
      // Hash the password
      const hashedPassword = await bcrypt.hash(req.body.password, saltRounds);
  
      // Create a new user
      const newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        userName: req.body.userName,
        password: hashedPassword,
        dateOfBirth: req.body.dateOfBirth,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
      });
  
      // Save the new user
      await newUser.save();
  
      res.status(201).send("Signup successful");
    } catch (err) {
      console.log(err);
      res.status(500).send("Signup failed");
    }
  }

  function generateAccessToken(userName) {
    return jwt.sign(userName, process.env.TOKEN_SECRET, { expiresIn: "1800s" });
  }

const postUserForLogin = async (req, res) => {
    try {
      const userName = req.body.userName;
      const users = await User.find({ userName: userName });
  
      if (users.length > 0) {
        const user = users[0];
        const password = req.body.password;
        console.log(password);
        const hashedPassword = user.password;
        console.log(hashedPassword);
        const comparison = await bcrypt.compare(password, hashedPassword);
  
        // res.json(comparison)
  
        if (comparison) {
          const dataToSaveInJWT = {
            _id: user._id,
          };
          if (user.userName === "admin") {
            dataToSaveInJWT.role = "admin";
          }
          const token = generateAccessToken(dataToSaveInJWT);
  
          res
            .cookie("jwt", token, {
              httpOnly: false,
              secure: false,
              maxAge: 1800 * 1000,
            })
            .json(token);
        }
      } else {
        throw "user not found";
      }
    } catch (err) {
      res.status(404).send("login Failed");
      console.log(err);
    }
  }

const deleteUserById = async (req, res) => {
    try {
      const users = await User.findByIdAndDelete(req.params.id);
      const a1 = await users.remove();
      res.json(a1);
    } catch (err) {
      res.send("Error in deleting");
    }
  }

module.exports = {
    getUsers,
    getUsersForUser,
    getUserById,
    postUser,
    postUserForLogin,
    deleteUserById,
}