const Review = require("../models/review")


const getReviews = async (req, res) => {
    try {
        const review = await Review.find(req.query);
        res.json(review);
    }catch (err) {
        res.status(500).send("Error: " + err);
    }
}

const getReviewsById = async (req, res) => {
    try {
        const review = await Review.findById(req.params.id);
        res.json(review);
    }catch (err) {
        res.status(500).send("Error: " + err);
    }
}

const postReview = async (req, res) => {
    const review = new Screen({
      movie: req.body.movie,
      user: req.body.user,
      rating: req.body.rating,
      review: req.body.review
    });
    try {
      const a1 = await Review.save();
      console.log(a1);
      res.json(a1);
    } catch (err) {
      console.log(err);
      res.status(404).send("Error "+ err )
    }
  };

const deleteReviewById = async (req, res)=> {
    try {
        const review = await Review.findByIdAndDelete(req.params.id);
        res.send("review deleted")
    }catch {
      console.log(err);
      res.status(404).send("Error "+ err )
    }
}

const deleteAllReviews = async (req, res) => {
    const deleteAll = await Review.remove();
    res.send("All reviews have been deleted");
  }


module.exports = {
    getReviews,
    getReviewsById,
    postReview,
    deleteReviewById,
    deleteAllReviews,
}