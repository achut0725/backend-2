const express = require("express");
const router = express.Router();

const CrewMember = require("../models/crewMember")

const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const { getCrewMembers, getCrewMembersById, postCrewMembers, putAnyChangesInCrewMembersDetails, deleteCrewMembers } = require("../controllers/CrewMemberController");


router.get("/", getCrewMembers);

router.get("/:id", getCrewMembersById);

router.post("/", verifyToken, roleCheck, postCrewMembers);

router.put("/:id", verifyToken, roleCheck, putAnyChangesInCrewMembersDetails);

router.delete("/:id", verifyToken, roleCheck, deleteCrewMembers);
  

module.exports = router;
