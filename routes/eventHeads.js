const express = require("express");
const router = express.Router();

const EventHead = require("../models/eventHead")

const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const { getEventHeads, getEventHeadsById, postEventHeads, deleteEventHeadsById } = require("../controllers/EventHeadsController");

router.get("/", getEventHeads);

router.get("/:id", getEventHeadsById);

router.post("/", verifyToken, roleCheck, postEventHeads);

router.delete("/:id", verifyToken, roleCheck, deleteEventHeadsById);


module.exports = router;