const express = require("express");
const router = express.Router();
const Screen = require("../models/screen");

const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const { getScreens, getScreenById, deleteScreenById, deleteAllScreens, postScreen, removeScreen } = require("../controllers/ScreenController");
const Screening = require("../models/screening");

router.get("/", getScreens);

router.get("/:id", getScreenById);

router.post("/", verifyToken, roleCheck, postScreen);

router.patch("/:id", verifyToken, roleCheck, removeScreen, async (req, res) => {
    try{
        const results = await Screening.updateMany({screen: req.params.id}, {status: false})
        console.log(results)
        res.status(200).send("removed")
    }
    catch(err){
        res.status(400).send("could not update related screenigs")
    }

});

router.delete("/:id", verifyToken, roleCheck, deleteScreenById, async (req, res) => {
    try{
        const results = await Screening.deleteMany({screen: req.params.id})
        console.log(results)
        res.status(200).send("deleted")
    }
    catch(err){
        res.status(400).send("could not update related screenigs")
    }
})

router.post("/all", verifyToken, roleCheck, deleteAllScreens);


module.exports = router;

