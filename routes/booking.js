const express = require("express");
const stripe = require("stripe")(
  "sk_test_51MwiBXSFetBKNIhxUA5eDcnCYzhc2C3GeOJbFj1PEw9i8JUJSJ1R4L4q1elZCSiz520eu8OzJayxSW3NYI4Iylv500eNeOu9U2"
);

const YOUR_DOMAIN = "http://localhost:3000";

const router = express.Router();

const User = require("../models/user");
const Ticket = require("../models/tickets");


var { verifyToken, roleCheck } = require("../controllers/auth-controller");
const { postBooking, getBookingForAdmin, putAnyChangesInTicket, cancelBookingForAdmin, cancelBookingForUser, getBookingForUser, getBookedTickets, getBookingsById, patchAnythingInTicket, deleteBookingById, deleteAllBookings } = require("../controllers/BookingController");


router.post("/", verifyToken, postBooking);


router.post("/create-checkout-session", async (req, res) => {
  console.log(req.body);
  const session = await stripe.checkout.sessions.create({
    line_items: [
      {
        // Provide the exact Price ID (for example, pr_1234) of the product you want to sell
        price: "price_1MzBv1SFetBKNIhxl0y5Erp8",
        quantity: req.body.quantity,
      },
    ],
    mode: "payment",
    success_url: `${YOUR_DOMAIN}/Tickets`,
    cancel_url: `${YOUR_DOMAIN}?canceled=true`,
    client_reference_id: req.body.ticketId,
  });

  // const confirmedBooking = await Ticket.findByIdAndUpdate(req.body.booking, {bookingStatus: true, paymentStatus: true}, {new:})

  res.json({ url: session.url });
});


router.get("/", verifyToken, roleCheck, getBookingForAdmin);

router.put("/:id", verifyToken, roleCheck, putAnyChangesInTicket);

router.patch("/:id", verifyToken, roleCheck, cancelBookingForAdmin);

router.patch("/:id/cancel", verifyToken, cancelBookingForUser);

router.get("/tickets", verifyToken, getBookingForUser);

router.get("/booked", getBookedTickets);

router.get("/:id", getBookingsById);

router.patch("/:id", patchAnythingInTicket);

router.delete("/:id", verifyToken, roleCheck, deleteBookingById);

router.post("/all", verifyToken, roleCheck, deleteAllBookings);


module.exports = router;
