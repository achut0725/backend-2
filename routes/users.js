const express = require("express");
const router = express.Router();

const User = require("../models/user");
const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const { getUsers, getUsersForUser, getUserById, postUser, postUserForLogin, deleteUserById } = require("../controllers/UserController");


router.get("/", verifyToken, roleCheck, getUsers);

router.get("/profile", verifyToken, getUsersForUser);

router.get("/:id", verifyToken, getUserById);

router.post("/", postUser);

router.post("/login", postUserForLogin);

router.delete("/:id", verifyToken, roleCheck, deleteUserById);



module.exports = router;