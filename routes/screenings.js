const express = require("express");
const router = express.Router();

const Screening = require("../models/screening");

const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const {
  getAllScreenings,
  getScreeningByscreeningId,
  getScreeningById,
  postScreening,
  cancelScreeningById,
  deleteScreeningById,
  deleteAllScreenings,
  deleteScreeningBySreen,
} = require("../controllers/ScreeningController");

router.get("/", getAllScreenings);

router.get("/:screeningId", getScreeningByscreeningId);

router.get(":id/", getScreeningById);

router.post("/", verifyToken, roleCheck, postScreening);

router.patch("/:id/cancel", verifyToken, roleCheck, cancelScreeningById);

router.delete("/:id", verifyToken, roleCheck, deleteScreeningById);

router.delete("/", verifyToken, roleCheck, deleteScreeningBySreen);

router.post("/all", verifyToken, roleCheck, deleteAllScreenings);

module.exports = router;
