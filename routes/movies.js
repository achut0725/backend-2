const express = require("express");
const router = express.Router();
const Movie = require("../models/movie");
const Screening = require("../models/screening")

const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const { getMovies, getMoviesById, postMovies, deleteMovieById, deleteAllMovies, removeMovie, putAnyChangesInMovieDetails } = require("../controllers/MovieController");


router.get("/", getMovies);

router.get("/:id", getMoviesById);

router.post("/", verifyToken, roleCheck, postMovies);

router.put("/:id", verifyToken, roleCheck, putAnyChangesInMovieDetails);

router.patch("/:id", verifyToken, roleCheck, removeMovie, async (req, res) => {
    try{
        const results = await Screening.updateMany({movie: req.params.id}, { status: false }, { new: true })
        console.log(results)
        res.status(200).send("Movie removed," + "Related screenings also removed")
    }
    catch(err){
        res.status(400).send("could not update related screenigs")
    }
});

router.delete("/:id", verifyToken, roleCheck, deleteMovieById, async (req, res) => {
    try{
        const results = await Screening.deleteMany({movie: req.params.id})
        console.log(results)
        res.status(200).send("Movie deleted," + "Related screenings also deleted")
    }
    catch(err){
        res.status(400).send("could not update related screenigs")
    }
})

router.post("/all", verifyToken, roleCheck, deleteAllMovies);

module.exports = router;
