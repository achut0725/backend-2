const express = require("express");
const router = express.Router();

const { verifyToken, roleCheck } = require("../controllers/auth-controller");
const Review = require("../models/review");
const { getReviews, getReviewsById, postReview, deleteReviewById, deleteAllReviews } = require("../controllers/ReviewController");


router.get("/", getReviews);

router.get("/:id", getReviewsById);

router.post("/", verifyToken, roleCheck, postReview);

router.delete("/:id", deleteReviewById) 

router.post("/all", verifyToken, roleCheck, deleteAllReviews)


module.exports = router;


