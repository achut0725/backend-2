const express = require("express");
const sendMail = require("./Utilities/Mail");
const stripe = require("stripe")(process.env.STRIPE_SECRET);

const mongoose = require("mongoose");
const url = "mongodb://localhost/CineplexBackend";

mongoose.connect(url, { useNewUrlParser: true });
const con = mongoose.connection;

var cors = require("cors");
const { Model } = require("./model");
// const { Booking } = require('./booking')

// const model = new Model();
// const booking = new Booking();

const app = express();
// const port = 3000

const Ticket = require("./models/tickets");

var cookieParser = require("cookie-parser");

con.on("open", () => {
  console.log("connected....");
});

app.use(
  cors({
    origin: [
      "http://localhost:3000",
      "http://localhost:1234",
      "http://localhost:3001",
    ],
    credentials: true,
  })
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const endpointSecret =
  "whsec_7612aaa087c9a7215b3d99e85b2470a112ff903fb0a4d82ba10ab113645e871e";

app.post(
  "/webhook",
  express.raw({ type: "application/json" }),
  async (request, response) => {
    const sig = request.headers["stripe-signature"];

    let event;

    try {
      event = stripe.webhooks.constructEvent(request.body, sig, endpointSecret);
    } catch (err) {
      response.status(400).send(`Webhook Error: ${err.message}`);
      return;
    }

    // Handle the event
    switch (event.type) {
      case "checkout.session.completed":
        const result = event.data.object;
        const ticketId = result.client_reference_id;

        const ticket = await Ticket.findByIdAndUpdate(
          ticketId,
          { paymentReference: result.id },
          { new: true }
        );
        console.log(ticket);
        const ticketDetails = await Ticket.findById(ticketId).populate("user");

        console.log(ticketDetails);

        await sendMail(ticketDetails);
        // Then define and call a function to handle the event payment_intent.succeeded
        break;
      // ... handle other event types
      default:
        console.log(`Unhandled event type ${event.type}`);
    }

    // Return a 200 response to acknowledge receipt of the event
    response.send();
  }
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.use(express.json());
app.use(cookieParser());

app.get("/mail", async (req, res) => {
  await sendMail();
  res.send();
});

const bookingRouter = require("./routes/booking");
app.use("/booking", bookingRouter);

const screensRouter = require("./routes/screens");
app.use("/screens", screensRouter);

const moviesRouter = require("./routes/movies");
app.use("/movies", moviesRouter);

const screeningsRouter = require("./routes/screenings");
app.use("/screenings", screeningsRouter);

const usersRouter = require("./routes/users");
app.use("/users", usersRouter);

const eventHeadsRouter = require("./routes/eventHeads");
app.use("/eventHeads", eventHeadsRouter)

const crewMembersRouter = require("./routes/crewMembers");
app.use("/crewMembers", crewMembersRouter)

const reviewsRouter = require("./routes/reviews");
app.use("/reviews", reviewsRouter)


app.listen(9000, () => {
  console.log("server started");
});
