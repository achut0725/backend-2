const formData = require("form-data");
const Mailgun = require("mailgun.js");
const mailgun = new Mailgun(formData);
const mg = mailgun.client({
  username: "api",
  key: "064cff7b3f52ce55178e40e2b3a9c558-181449aa-1d30648b",
});

async function sendMail(ticketDetails) {
  console.log(ticketDetails.user.email);
  try {
    await mg.messages.create(
      "sandbox32f3aae07c8c4c4581fdd68f2a9439bc.mailgun.org",
      {
        from: "Mailgun Sandbox <mailgun@sandbox32f3aae07c8c4c4581fdd68f2a9439bc.mailgun.org>",
        to: ["aswinachut007@gmail.com"],
        subject: "Hello Mr ",
        text: "Your ticket for -movie- screening at -time- has been successfully booked",
        html: "<h1>Testing some Mailgun awesomeness!</h1>"
      }
    );
  } catch (err) {
    console.log("Error while sending email" + err);
  }
}

module.exports = sendMail;

//${ticketDetails.screening.movie.movieName}  ${ticketDetails.screening.time}  ${ticketDetails.user.firstName}

// You can see a record of this email in your logs: https://app.mailgun.com/app/logs.

// You can send up to 300 emails/day from this sandbox server.
// Next, you should add your own domain so you can send 10000 emails/month for free.
