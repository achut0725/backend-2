const mongoose = require("mongoose");

const reviewSchema = new mongoose.Schema({
  review: {
    movie: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Movie"
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    rating: {
      type: Number,
    },
    review: {
      type: String,
    },
  },
});

module.exports = mongoose.model("Review", reviewSchema);
