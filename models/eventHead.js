const mongoose = require("mongoose");

const eventHeadSchema = new mongoose.Schema({

    eventHeadName: {
        type: String,
        required: true,
    },
    discription: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    },
    // events: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "Event",
    // }

});


module.exports = mongoose.model("EventHead", eventHeadSchema);