const mongoose = require("mongoose");

const screeningSchema = new mongoose.Schema({
  movie: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Movie",
    require: true,
  },
  screen: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Screen",
    require: true,
  },
  time: {
    type: Date,
    require: true,
  },
  status: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model("Screening", screeningSchema);

// 63a3db025b620bed2f882683 - Knthara - movie
// 63a1395636e90567eae7d52a - 2 - screenNumber

// 63a3db8d5b620bed2f882685 - KGF - movie
// 63a1396e36e90567eae7d52c - 4 - screenNumber

// new ->

// 63ae5c59dc2cc762b68abd67 - Kanthara
// 63b4eda7ebe9ae50a5b971d7 - Avatar 2
// 63b4f055ebe9ae50a5b971e0 - Kaapa
