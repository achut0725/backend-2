const mongoose = require("mongoose")

const crewMemberSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
    },
    bio: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    }

})

module.exports = mongoose.model("CrewMember", crewMemberSchema);
