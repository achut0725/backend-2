const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema({
  
  movieName: {
    type: String,
    required: true,
  },
  discription: {
    type: String,
    required: true,
  },
  duration: {
    type: String,
    required: true,
  },
  summery: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  backgroundImage: {
    type: String,
    required: true,
  },
  rating: {
    type: String,
    required: true
  },
  moreInfo: {
    type: String,
    required: true,
  },
  castMembers: [
    {
      cast: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CrewMember",
        required: true,
      },
      castAs: {
        type: String,
        required: true,
      },
    },
  ],
  crewMembers: [
    {
      crew: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CrewMember",
        required: true,
      },
      crewAs: {
        type: String,
        required: true,
      },
    },
  ],
  status: {
    type: Boolean,
    default: true,
  },
});


movieSchema.pre("remove", function (next) {
  Screening.remove({ screening_id: this._id }).exec();
  next();
});

module.exports = mongoose.model("Movie", movieSchema);
