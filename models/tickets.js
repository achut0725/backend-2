const mongoose = require("mongoose");

const ticketSchema = new mongoose.Schema({
  user: {
    type: mongoose.Types.ObjectId,
    ref: "User",
    required: true,
  },
  // bookingStatus: {
  //   type: Boolean,
  //   default: true,
  // },
  bookingStatus: {
    type: String,
    default: "Booked",
    enum: ["Booked", "Cancelled by User", "Cancelled by Admin"]
  },
  paymentReference: {
    type: String,
  },
  screening: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Screening",
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  seatsSelected: [
    {
      rowId: {
        type: String,
        required: true,
      },
      seat: {
        type: Number,
        required: true,
      },
    },
  ],
  // createdTime: {
  //     type: Date,
  //     default: Date.now
  // }

  // seatsSelected: {
  //     type: [{
  //         required: true,
  //         type: Number
  //     }],
  //     validate: [checkMinimumOneSeatSelected, 'At least one seat  must be selected']
  // }
});
function checkMinimumOneSeatSelected(array) {
  return array.length >= 1;
}

module.exports = mongoose.model("Ticket", ticketSchema);
