const mongoose = require("mongoose");

const screenSchema = new mongoose.Schema({
  screenNumber: {
    type: String,
    unique: true,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  rows: [
    {
      rowId: {
        type: String,
        required: true,
      },
      noOfSeats: {
        type: Number,
        required: true,
      },
    },
  ],
  status: {
    type: Boolean,
    default: true
  },
  
});

// screenSchema.pre("remove", function (next) {
//   console.log("pre");
//   Screening.remove({ screen: this._id }).exec();
//   next();
// });

module.exports = mongoose.model("Screen", screenSchema);

// 63d0a88fcf1a2251a76c08e2
// 63d0a894cf1a2251a76c08e4
// 63dc8c06170e07241aa70fd9
// 63dc8c11170e07241aa70fdb
// 63dc8c16170e07241aa70fdd
// 63dc8c97170e07241aa70fe0
// 63dc8ca0170e07241aa70fe2
// 63dc8cd3170e07241aa70fe4
// 63dc8ce5170e07241aa70fe6
